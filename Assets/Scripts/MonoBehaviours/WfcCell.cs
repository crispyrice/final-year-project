using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WfcCell : MonoBehaviour
{
    public uint up;
    public uint down;
    public uint left;
    public uint right;

    [Range(1, 100)] public uint weight;

    public Dictionary<Vector2Int, uint> Sockets =>
        new()
        {
            { Vector2Int.up, up },
            { Vector2Int.down, down },
            { Vector2Int.left, left },
            { Vector2Int.right, right }
        };
}