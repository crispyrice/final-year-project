using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MeshCreator : MonoBehaviour
{
    private readonly MapDataGenerator _mapDataGenerator = new();

    private WfcMap _wfcMap;

    [Range(5, 25)] public int chunkSize = 10;
    [Range(25, 200)] public int citySize = 50;
    [Range(0, 1)] public float residentialDensityThreshold = 0.5f;
    [Range(0, 1)] public float commercialDensityThreshold = 0.5f;
    [Range(0, 1)] public float densityFalloff = 0.5f;
    public int seed;
    public bool autoRegen;

    private WfcCell GenerateChunk(Vector2Int position)
    {
        float heightValue = _mapDataGenerator.GetBuildingHeight(
            position.x,
            position.y,
            chunkSize, chunkSize
        );
        Color chunkColor = _mapDataGenerator.GetBuildingColor(
            position.x,
            position.y,
            chunkSize, chunkSize
        );

        // Copy the cell into this scene
        WfcCell cell = Instantiate(_wfcMap.GetCellAt(position.x / chunkSize, position.y / chunkSize),
            transform);

        // Set the cell's position in the grid
        Transform cellTransform = cell.transform;
        cellTransform.position = new Vector3(
            position.x,
            0,
            position.y
        );
        cellTransform.localScale = new Vector3(chunkSize / 2.0f, chunkSize / 2.0f, heightValue);

        // Set the cell's color
        MeshRenderer meshRenderer = cell.GetComponent<MeshRenderer>();
        Material cellMaterial = new(meshRenderer.sharedMaterial);
        cellMaterial.color = chunkColor;
        meshRenderer.material = cellMaterial;

        return cell;
    }

    public void GenerateCity()
    {
        WfcCell[] cells = SceneManager.GetSceneByName("Meshes").GetRootGameObjects()
            .Select(c => c.GetComponent<WfcCell>()).ToArray();

        _wfcMap = new WfcMap(cells, Mathf.CeilToInt(citySize / (float)chunkSize),
            Mathf.CeilToInt(citySize / (float)chunkSize), seed);

        _mapDataGenerator.Seed = seed;
        _mapDataGenerator.Width = citySize;
        _mapDataGenerator.Height = citySize;
        _mapDataGenerator.ResidentialDensityThreshold = residentialDensityThreshold;
        _mapDataGenerator.CommercialDensityThreshold = commercialDensityThreshold;
        _mapDataGenerator.DensityFalloff = densityFalloff;

        // Remove all chunks
        while (transform.childCount > 0)
            DestroyImmediate(transform.GetChild(0).gameObject);

        Debug.Log("Building Chunks");
        for (float x = 0; x < _mapDataGenerator.Width; x += chunkSize)
        for (float y = 0; y < _mapDataGenerator.Height; y += chunkSize)
        {
            WfcCell chunk = GenerateChunk(new Vector2Int(Mathf.FloorToInt(x), Mathf.FloorToInt(y)));
            chunk.name = $"Chunk ({Mathf.FloorToInt(x / chunkSize)}, {Mathf.FloorToInt(y / chunkSize)})";
        }
    }
}