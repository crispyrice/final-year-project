using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class WfcMap
{
    private int Width { get; }
    private int Height { get; }
    private int Seed { get; }
    private bool Generated { get; set; }

    /// <summary>
    /// A grid of cell possibilities.
    ///
    /// Each element in the grid can contain multiple cells.
    ///
    /// If an element has only one cell, then that element is collapsed.
    /// </summary>
    private PossibleCells[,] Grid { get; set; }

    public WfcMap(IReadOnlyCollection<WfcCell> cells, int width, int height, int seed)
    {
        Width = width;
        Height = height;
        Seed = seed;
        Generated = false;

        Grid = new PossibleCells[width, height];
        for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++)
            Grid[x, y] = new PossibleCells(cells);
    }


    /// <summary>
    /// Recursive collapse function.
    /// </summary>
    /// <param name="position">The position in the grid that was chosen</param>
    /// <param name="chosenCell">The cell that was chosen for the original</param>
    /// <param name="grid">The grid (readonly) to collapse</param>
    private PossibleCells[,] CollapseGrid(Vector2Int position, WfcCell chosenCell, PossibleCells[,] grid)
    {
        // Clone the grid
        PossibleCells[,] gridClone = new PossibleCells[Width, Height];
        for (int y = 0; y < Height; y++)
        for (int x = 0; x < Width; x++)
            gridClone[x, y] = new PossibleCells(grid[x, y]);

        // Choose the cell in the clone's grid position
        gridClone[position.x, position.y].Choose(chosenCell);
        gridClone[position.x, position.y].Explored = true;

        foreach (Vector2Int direction in new[] { Vector2Int.up, Vector2Int.down, Vector2Int.left, Vector2Int.right })
        {
            Vector2Int neighbour = position + direction;
            // Check if the neighbour is within the grid
            if (neighbour.x < 0 || neighbour.x >= Width || neighbour.y < 0 || neighbour.y >= Height
                || gridClone[neighbour.x, neighbour.y].Explored)
                continue;

            // Remove all not possible neighbours
            int numRemoved = gridClone[neighbour.x, neighbour.y].CollapseGiven(chosenCell, direction);

            // Don't recurse if nothing changed
            if (numRemoved <= 0) continue;
            // Return null (indicating failure) if there were no possible neighbours
            if (gridClone[neighbour.x, neighbour.y].IsEmpty)
                return null;

            // Update the grid clone if some neighbours need collapsing
            PossibleCells[,] g = SetCellAt(neighbour.x, neighbour.y, gridClone);
            if (g != null) gridClone = g; // Discard the result if it's null
        }

        return gridClone;
    }

    /// <summary>
    /// Collapses the cell at the given coordinates using a random number generator
    /// </summary>
    /// <param name="x">The X coordinate on the WFC Grid</param>
    /// <param name="y">The Y coordinate on the WFC Grid</param>
    /// <param name="grid"></param>
    private PossibleCells[,] SetCellAt(int x, int y, PossibleCells[,] grid)
    {
        // Clone the grid
        PossibleCells[,] gridClone = new PossibleCells[Width, Height];
        for (int yy = 0; yy < Height; yy++)
        for (int xx = 0; xx < Width; xx++)
            gridClone[xx, yy] = new PossibleCells(grid[xx, yy]);

        // Try every possible cell until one works
        while (true)
        {
            // Get a random cell from the list
            List<WfcCell> weightedList = new();

            foreach (WfcCell cell in gridClone[x, y])
                weightedList.AddRange(Enumerable.Repeat(cell, (int)cell.weight));
            int index = Random.Range(0, weightedList.Count);
            WfcCell possibleCell = weightedList[index];

            // Collapse the grid
            PossibleCells[,] possibleGrid = CollapseGrid(new Vector2Int(x, y), possibleCell, gridClone);
            if (possibleGrid != null)
                return possibleGrid;

            // Remove the cell from the list
            gridClone[x, y].RemoveAt(index);
        }
    }

    /// <summary>
    /// Generate the grid
    /// </summary>
    private void Generate()
    {
        // Initialize Random here to ensure consistency
        Random.InitState(Seed);

        // Choose cells to set using a semi-random process
        while (true)
        {
            // Choose a cell with the lowest number of possibilities
            int lowestDomain = int.MaxValue;
            Vector2Int lowestDomainPosition = new(-1, -1);
            for (int y = 0; y < Height; y++)
            for (int x = 0; x < Width; x++)
                if (Grid[x, y].Count != 1 && Grid[x, y].Count < lowestDomain)
                {
                    lowestDomain = Grid[x, y].Count;
                    lowestDomainPosition = new Vector2Int(x, y);
                }

            // If all cells have a domain of 1, the grid is fully collapsed, so we can stop
            if (lowestDomain == int.MaxValue || lowestDomainPosition == new Vector2Int(-1, -1)) break;

            // Set the cell at the lowest domain position
            Grid = SetCellAt(lowestDomainPosition.x, lowestDomainPosition.y, Grid);
        }
    }

    public WfcCell GetCellAt(int x, int y)
    {
        if (Generated) return Grid[x, y][0];

        Generate();
        Generated = true;

        return Grid[x, y][0];
    }
}