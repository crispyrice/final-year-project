using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PossibleCells : IEnumerable<WfcCell>
{
    private readonly List<WfcCell> _cells;

    public bool Explored { get; set; }
    public bool IsEmpty => _cells.Count == 0;
    public int Count => _cells.Count;

    public PossibleCells(IReadOnlyCollection<WfcCell> cells, bool explored = false)
    {
        // Each list is unique, but the cell references are shared
        // This saves quite a bit of memory
        this._cells = new List<WfcCell>();
        _cells.AddRange(cells);
        Explored = explored;
    }

    public PossibleCells(PossibleCells from) : this(from._cells, from.Explored) { }

    /// <summary>
    /// Remove all other cells from this grid position
    /// </summary>
    /// <param name="chosenCell">The cell to choose</param>
    public void Choose(WfcCell chosenCell)
    {
        // Reuse the list object to save memory
        _cells.Clear();
        _cells.Add(chosenCell);
    }

    /// <summary>
    /// Collapse this cell possibility set given a neighbour cell
    /// </summary>
    /// <param name="originCell"></param>
    /// <param name="direction"></param>
    public int CollapseGiven(WfcCell originCell, Vector2Int direction)
        => _cells.RemoveAll(possible => possible.Sockets[direction] != originCell.Sockets[-direction]);

    public void RemoveAt(int index) => _cells.RemoveAt(index);
    public WfcCell this[int index] => _cells[index];

    public IEnumerator<WfcCell> GetEnumerator() => _cells.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
