using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MapDataGenerator
{
    private enum Zone
    {
        HighDensityCommercial,
        LowDensityCommercial,
        HighDensityResidential,
        LowDensityResidential,
    }

    private static readonly Dictionary<Zone, Vector2> zoneWeights = new()
    {
        { Zone.HighDensityCommercial, new(5, 15) },
        { Zone.LowDensityCommercial, new(1, 6) },
        { Zone.HighDensityResidential, new(7, 22) },
        { Zone.LowDensityResidential, new(0.5f, 3) },
    };

    public float Width { get; set; }
    public float Height { get; set; }
    public float ResidentialDensityThreshold { get; set; }
    public float CommercialDensityThreshold { get; set; }
    public float DensityFalloff { get; set; }
    public float Seed { get; set; }

    private float Sigmoid(float x, float alpha, float beta)
    {
        return Mathf.Pow(x, alpha) / (Mathf.Pow(x, alpha) + Mathf.Pow(beta - beta * x, alpha));
    }

    private Zone GetBuildingZone(float x, float y, float width, float height)
    {
        const float zoneSize = 30;
        float xw = (x + width / 2) / zoneSize;
        float yh = (y + height / 2) / zoneSize;

        float closenessToEdge =
            (Mathf.Abs(Mathf.InverseLerp(0, Width, x + width / 2) * 2 - 1) +
            Mathf.Abs(Mathf.InverseLerp(0, Height, y + height / 2) * 2 - 1)) / 2;

        float falloff = Sigmoid(closenessToEdge, 3, DensityFalloff);

        float residentialSeed = Seed * 300;
        float residentialNoise = Mathf.PerlinNoise(residentialSeed + xw, residentialSeed + yh);
        float commercialSeed = Seed * 200;
        float commercialNoise = Mathf.PerlinNoise(commercialSeed + xw, commercialSeed + yh);
        float densitySeed = Seed * 100;
        float densityNoise = Mathf.PerlinNoise(densitySeed + xw, densitySeed + yh);

        if (residentialNoise > commercialNoise)
        {
            if (densityNoise >= ResidentialDensityThreshold / falloff) return Zone.HighDensityResidential;
            else return Zone.LowDensityResidential;
        } else
        {
            if (densityNoise >= CommercialDensityThreshold / falloff) return Zone.HighDensityCommercial;
            else return Zone.LowDensityCommercial;
        }
    }

    public float GetBuildingHeight(float x, float y, float width, float height)
    {
        Vector2 weights = zoneWeights[GetBuildingZone(x, y, width, height)];
        return Mathf.Lerp(weights.x, weights.y,
            Mathf.PerlinNoise(Seed + x + width / 2, Seed + y + height / 2));
    }

    public Color GetBuildingColor(float x, float y, float width, float height)
    {
        Zone z = GetBuildingZone(x, y, width, height);
        return z switch
        {
            Zone.HighDensityCommercial => Color.blue,
            Zone.LowDensityCommercial => Color.cyan,
            Zone.HighDensityResidential => Color.yellow,
            Zone.LowDensityResidential => Color.green,
            _ => new Color(0.5f, 0.5f, 0.5f),
        };
    }
}
