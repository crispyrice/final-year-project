using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshCreator))]
public class MeshCreatorEditor : Editor
{
    private MeshCreator TargetMeshCreator { get { return target as MeshCreator; } }

    public override void OnInspectorGUI()
    {
        if (DrawDefaultInspector())
        {
            if (TargetMeshCreator.autoRegen)
                TargetMeshCreator.GenerateCity();
        }
        if (!TargetMeshCreator.autoRegen && GUILayout.Button("Regenerate"))
            TargetMeshCreator.GenerateCity();
    }
}
